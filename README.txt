SSD1306xLED - Drivers for SSD1306 controlled dot matrix OLED/PLED 128x64 displays

SSD1306xLED is a C library for working with the SSD1306 display driver to control dot matrix OLED/PLED 128x64 displays. It is intended to be used with the Tinusaur board but should also work with any other board based on ATtiny85 or similar microcontroller.

SSD1306xLED is written in plain C and does not require any additional libraries to function except those that come with the WinAVR SDK.

-----------------------------------------------------------------------------------
 Copyright (c) 2015 Neven Boyanov, Tinusaur Team. All Rights Reserved.

 Distributed as open source software under MIT License, see LICENSE.txt file.

 Please, as a favour, retain the link http://tinusaur.org to The Tinusaur Project.

-----------------------------------------------------------------------------------


